package com.disysy.myapplication.utils;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.Map;


public class NetworkRequest
{

    private static final NetworkRequest ourInstance = new NetworkRequest();

    public static NetworkRequest getInstance()
    {
        return ourInstance;
    }

    private final static  String BASE_URL = "https://api.qa.mrhe.gov.ae/mrhecloud/v1.4/api";
    private final static  String VALIDATE_USER = "iskan/v1/certificates/towhomitmayconcern";
    private final static  String NEWS_URL = "public/v1/news?local=en";

    private NetworkRequest()
    {

    }

    public  String getLoginValidateURL()
    {
        StringBuilder urlBuilder = new StringBuilder(BASE_URL);
        urlBuilder.append("/");
        urlBuilder.append(VALIDATE_USER);
        return urlBuilder.toString();
    }
    public  String getNewsURL()
    {
        StringBuilder urlBuilder = new StringBuilder(BASE_URL);
        urlBuilder.append("/");
        urlBuilder.append(NEWS_URL);
        return urlBuilder.toString();
    }

    /****
     *
     *
     * function responsible for performing request to
     * server on completion it will call  OnRequestCompleteListener
     *
     * @param context
     * @param requestType
     * @param url
     * @param headersparams
     * @param inputParams
     * @param completeListener
     */


    public  void  PerformRequest(final Context context, int requestType , String url, final Map<String,String> headersparams,
                                 final Map<String, String> inputParams, final OnRequestCompleteListener completeListener)
    {

        RequestQueue queue = Volley.newRequestQueue(context);
        StringRequest request = new StringRequest(requestType, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                completeListener.responseData(response);

            }
        },
        new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {


                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                {

                    completeListener.timeOutError();

                }
                else if (error instanceof AuthFailureError)
                {

                    completeListener.authFailureError();

                } else if (error instanceof ServerError) {

                    completeListener.serverError();

                } else if (error instanceof NetworkError) {

                    completeListener.networkError();

                } else if (error instanceof ParseError) {
                    completeListener.parseError();
                }


            }

        }


        )

        {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError
            {
                return headersparams;
            }
            @Override
                protected Map<String, String> getParams() {
                return inputParams;
            }

        };

        queue.add(request);
    }








}

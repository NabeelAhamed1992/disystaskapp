package com.disysy.myapplication.utils;

import com.android.volley.VolleyError;

public abstract class OnRequestCompleteListener {


    public abstract void responseData(String response);
    public abstract void serverError();
    public abstract void timeOutError();
    public abstract void authFailureError();
    public abstract void parseError();
    public abstract void networkError();
}

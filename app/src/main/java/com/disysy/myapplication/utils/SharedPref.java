package com.disysy.myapplication.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by NABEEL
 */

public class SharedPref
{

    private static String STRING_PREF = "MyApp";
    private SharedPreferences shared;
    private SharedPreferences.Editor editor;

    public SharedPref(Context context) {
        shared = context.getSharedPreferences(STRING_PREF, Context.MODE_PRIVATE);

    }

    public void setString(String key, String value) {
        editor = shared.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public String getString(String key) {
        return shared.getString(key, null);
    }
    public void setBoolean(String key, boolean value) {
        editor = shared.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key,boolean defValue) {
        return shared.getBoolean(key, defValue);
    }

    public void setInt(String key, int value) {
        editor = shared.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public int getInt(String key) {

        return shared.getInt(key, 0);
    }

    public void clearAll() {
        editor = shared.edit();
        editor.clear().commit();
    }



}


package com.disysy.myapplication.model;

public class User {





    private int id;
    private String employeeName;
    private int idBarahNo;
    private String emailAddress;
    private int unifiedNo;
    private String mobileNumber;

    public static String consumer_secret = "20891a1b4504ddc33d42501f9c8d2215fbe85008";
    public static String consumer_key = "mobile_dev";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public int getIdBarahNo() {
        return idBarahNo;
    }

    public void setIdBarahNo(int idBarahNo) {
        this.idBarahNo = idBarahNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public int getUnifiedNo() {
        return unifiedNo;
    }

    public void setUnifiedNo(int unifiedNo) {
        this.unifiedNo = unifiedNo;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }



}

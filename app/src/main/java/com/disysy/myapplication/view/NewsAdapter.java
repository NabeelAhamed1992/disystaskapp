package com.disysy.myapplication.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.disysy.myapplication.R;
import com.disysy.myapplication.model.NewsModel;


import java.util.ArrayList;
import java.util.List;


public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MyViewHolder>  {
    private Context context;
    private List<NewsModel> newsModelList;


    public NewsAdapter(Context context, ArrayList<NewsModel> newsModelArrayList)
    {
        this.context = context;
        this.newsModelList = newsModelArrayList;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, desc,date;
        ImageView newsImageView;


        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.title);
            desc = view.findViewById(R.id.desc);
            date= view.findViewById(R.id.date);
            newsImageView = view.findViewById(R.id.new_img);


        }
    }




    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position)
    {

        NewsModel newsModel = newsModelList.get(position);

        holder.title.setText(newsModel.getTitle());
        holder.desc.setText(newsModel.getDescription());
        holder.date.setText(newsModel.getDate());
        Glide.with(context).load(newsModel.getImageURL()).into(holder.newsImageView);




    }

    @Override
    public int getItemCount()
    {
       return newsModelList.size();

    }


}

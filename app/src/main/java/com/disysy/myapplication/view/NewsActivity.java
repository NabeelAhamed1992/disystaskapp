package com.disysy.myapplication.view;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.AllianceLoader;
import com.android.volley.Request;
import com.disysy.myapplication.utils.NetworkRequest;
import com.disysy.myapplication.utils.OnRequestCompleteListener;
import com.disysy.myapplication.R;
import com.disysy.myapplication.model.NewsModel;
import com.disysy.myapplication.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class NewsActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    AllianceLoader loader;
    String TAG = "news";
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_news);

        setTitle(R.string.news);




        fetchNews();

        findViews();



    }

    void findViews()
    {
        loader = findViewById(R.id.loader);
        recyclerView = findViewById(R.id.recycler_view);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchNews();
            }
        });

    }



    private void  UpdateUI(ArrayList<NewsModel> list)
    {
        NewsAdapter mAdapter = new NewsAdapter(this, list);
        recyclerView.setAdapter(mAdapter);

    }

    void dismissLoaderAndRefresh()
    {
        if(loader != null)
        {
            loader.setVisibility(View.GONE);
        }
        if(mSwipeRefreshLayout !=null)
        {
            mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    void fetchNews()
    {

        Map<String,String> headers = new HashMap<>();
        headers.put(getString(R.string.consumer_key), User.consumer_key);
        headers.put(getString(R.string.consumer_secret),User.consumer_secret);

        NetworkRequest requestOp  = NetworkRequest.getInstance();
          requestOp.PerformRequest(this, Request.Method.GET,
                requestOp.getNewsURL(), headers,null,
                new OnRequestCompleteListener()
                {

                    @Override
                    public void responseData(String response)
                    {
                        Log.e("RESPONSE",response);
                        handleResponse(response);
                        dismissLoaderAndRefresh();


                    }

                    @Override
                    public void serverError()
                    {
                        Log.e(TAG,"SERVER ERROR");
                        Toast.makeText(NewsActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();
                        dismissLoaderAndRefresh();
                    }

                    @Override
                    public void timeOutError() {
                        Log.e(TAG,"TIME OUT");
                        Toast.makeText(NewsActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();
                        dismissLoaderAndRefresh();
                    }

                    @Override
                    public void authFailureError() {
                        Log.e(TAG,"AUTH FAILURE ERROR");
                        Toast.makeText(NewsActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();
                        dismissLoaderAndRefresh();
                    }

                    @Override
                    public void parseError() {
                        Log.e(TAG,"PARSER ERROR");
                        Toast.makeText(NewsActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();
                        dismissLoaderAndRefresh();


                    }

                    @Override
                    public void networkError()
                    {

                        Log.e(TAG,"PARSER ERROR");
                        Toast.makeText(NewsActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();
                        dismissLoaderAndRefresh();

                    }
                });


    }

    private void handleResponse(String response) {
        try
        {

            JSONObject jsonObject = new JSONObject(response);
            boolean success = jsonObject.getBoolean(getString(R.string.success));
            if(success)
            {
                JSONArray dataArray = jsonObject.getJSONArray(getString(R.string.payload));
                ArrayList<NewsModel> newsModelArrayList = new ArrayList<>();
                for(int i =0;i<=dataArray.length()-1;i++)
                {
                     JSONObject newJSONObj = dataArray.getJSONObject(i);

                     NewsModel newsModel = new NewsModel();
                     newsModel.setTitle(newJSONObj.getString(getString(R.string.title_news_params)));
                     newsModel.setImageURL(newJSONObj.getString(getString(R.string.image_news_params)));
                     newsModel.setDate(newJSONObj.getString(getString(R.string.date_news_params)));
                     newsModel.setDescription(newJSONObj.getString(getString(R.string.desc_news_params)));

                     newsModelArrayList.add(newsModel);

                }

                UpdateUI(newsModelArrayList);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}

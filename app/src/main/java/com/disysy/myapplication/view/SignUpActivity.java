package com.disysy.myapplication.view;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.agrawalsuneet.dotsloader.loaders.AllianceLoader;
import com.android.volley.Request;
import com.disysy.myapplication.utils.NetworkRequest;
import com.disysy.myapplication.utils.OnRequestCompleteListener;
import com.disysy.myapplication.R;
import com.disysy.myapplication.utils.SharedPref;
import com.disysy.myapplication.utils.Utils;
import com.disysy.myapplication.model.User;
import com.libizo.CustomEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {


    private CustomEditText empId;
    private CustomEditText ename;
    private CustomEditText idBarahNo;
    private CustomEditText emailAddress;
    private CustomEditText unifiedNumber;
    private CustomEditText mobileNumber;
    private RelativeLayout signUp;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);

        checkAndStartNews();

        setTitle(getString(R.string.sign_up));

        findViews();




    }

    private void checkAndStartNews() {

        SharedPref sharedPref = new SharedPref(this);
        if(sharedPref.getBoolean("IsLogin",false))
        {
            startNewsActivity();
            finish();
        }
    }


    /**
     *
     */
    private void findViews() {

        empId = (CustomEditText)findViewById( R.id.emp_id );
        ename = (CustomEditText)findViewById( R.id.ename );
        idBarahNo = (CustomEditText)findViewById( R.id.id_barah_no );
        emailAddress = (CustomEditText)findViewById( R.id.email_address );
        unifiedNumber = (CustomEditText)findViewById( R.id.unified_number );
        mobileNumber = (CustomEditText)findViewById( R.id.mobile_number );
        signUp = (RelativeLayout)findViewById( R.id.sign_up );
        signUp.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        switch (v.getId())
        {
            case R.id.sign_up:
                signup();
                break;
        }

    }

    boolean validateForSignUp()
    {
        boolean checkForOverallValidation = true;

        if(!Utils.checkForNullAndLength(empId.getText().toString()))
        {
            checkForOverallValidation = false;

            empId.setError(getString(R.string.required_str));
        }
        if(!Utils.checkForNullAndLength(ename.getText().toString()))
        {
            checkForOverallValidation = false;
            ename.setError(getString(R.string.required_str));
        }
        if(!Utils.checkForNullAndLength(idBarahNo.getText().toString()))
        {
            checkForOverallValidation = false;
            idBarahNo.setError(getString(R.string.required_str));
        }
        if(!Utils.checkForNullAndLength(emailAddress.getText().toString()))
        {
            checkForOverallValidation = false;
            emailAddress.setError(getString(R.string.required_str));
        }
        if(!Utils.checkForNullAndLength(unifiedNumber.getText().toString()))
        {
            checkForOverallValidation = false;
            unifiedNumber.setError(getString(R.string.required_str));
        }

        if(!Utils.checkForNullAndLength(mobileNumber.getText().toString()))
        {
            checkForOverallValidation = false;
            mobileNumber.setError(getString(R.string.required_str));
        }
        return checkForOverallValidation;


    }



    private void signup()
    {

        if(validateForSignUp())
        {
            showLoader();
            performSignUpRequest();
        }

    }
    Dialog dialog;
    private void showLoader() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.loader_dailog);
        dialog.setCancelable(false);
        dialog.show();


    }

    void dismissLoader()
    {
        if(dialog != null)
        {
            dialog.dismiss();
        }
    }

    void performSignUpRequest()
    {
        NetworkRequest requestOp  = NetworkRequest.getInstance();


        Map<String,String> headers = new HashMap<>();
        headers.put(getString(R.string.consumer_key), User.consumer_key);
        headers.put(getString(R.string.consumer_secret),User.consumer_secret);


        Map<String,String> inputParams = new HashMap<>();
        inputParams.put(getString(R.string.eid_params),empId.getText().toString());
        inputParams.put(getString(R.string.name_params),ename.getText().toString());
        inputParams.put(getString(R.string.id_barano_params),idBarahNo.getText().toString());
        inputParams.put(getString(R.string.email_address_params),emailAddress.getText().toString());
        inputParams.put(getString(R.string.unified_number_params),unifiedNumber.getText().toString());
        inputParams.put(getString(R.string.mobile_no_params),mobileNumber.getText().toString());



        requestOp.PerformRequest(this, Request.Method.POST,
                requestOp.getLoginValidateURL(), headers,inputParams,
                new OnRequestCompleteListener()
                {

                    @Override
                    public void responseData(String response)
                    {
                        Log.e("RESPONSE",response);
                        dismissLoader();
                        handleResponse(response);


                    }

                    @Override
                    public void serverError()
                    {
                        Log.e("SIGNUP","SERVER ERROR");
                        dismissLoader();
                        Toast.makeText(SignUpActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void timeOutError() {
                        Log.e("SIGNUP","TIME OUT");
                        dismissLoader();
                        Toast.makeText(SignUpActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void authFailureError() {
                        Log.e("SIGNUP","AUTH FAILURE ERROR");
                        dismissLoader();
                        Toast.makeText(SignUpActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void parseError() {
                        Log.e("SIGNUP","PARSER ERROR");
                        dismissLoader();
                        Toast.makeText(SignUpActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();


                    }

                    @Override
                    public void networkError()
                    {

                        Log.e("SIGNUP","PARSER ERROR");
                        dismissLoader();
                        Toast.makeText(SignUpActivity.this,R.string.request_error_show,Toast.LENGTH_LONG).show();

                    }
                });
    }

    private void handleResponse(String response)
    {
        try {
            JSONObject jsonObject = new JSONObject(response);
            boolean success = jsonObject.getBoolean(getString(R.string.success));
            if(success)
            {

                SharedPref sharedPref = new SharedPref(this);
                sharedPref.setBoolean("IsLogin",true);
                saveUserDetails(sharedPref);
                startNewsActivity();
                finish();




            }
            else if(!success)
            {
                String message = jsonObject.optString(getString(R.string.message_str));
                Toast.makeText(this,message,Toast.LENGTH_SHORT).show();


            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void startNewsActivity() {
        Intent i = new Intent(this,NewsActivity.class);
        startActivity(i);
    }


    void saveUserDetails(SharedPref sharedPref)
    {


        sharedPref.setString(getString(R.string.eid_params),empId.getText().toString());
        sharedPref.setString(getString(R.string.name_params),ename.getText().toString());
        sharedPref.setString(getString(R.string.id_barano_params),idBarahNo.getText().toString());
        sharedPref.setString(getString(R.string.email_address_params),emailAddress.getText().toString());
        sharedPref.setString(getString(R.string.unified_number_params),unifiedNumber.getText().toString());
        sharedPref.setString(getString(R.string.mobile_no_params),mobileNumber.getText().toString());



    }
}
